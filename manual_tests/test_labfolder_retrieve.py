#!/usr/bin/env python3
#
# This file is a part of the CaosDB Project.
#
# Copyright (c) 2020 IndiScale GmbH
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

""" Retrieve Labfolder data from API """

import argparse
import sys

import caosmodels
from caosmodels.parser import parse_model_from_yaml

from caosadvancedtools.converter.labfolder_api import Importer


def main(args):
    """The main function."""
    model = parse_model_from_yaml("./model.yml")

    # model.sync_data_model()
    importer = Importer()
    importer.import_data()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("folder", default="./example_labfolder_data",
                        nargs="?", help='folder that contains the data')
    args = parser.parse_args()
    sys.exit(main(args))

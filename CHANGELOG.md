# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased] ##

### Added ###

- Error handling for missing files when using the crawler
- included the scifolder module
- included the caosmodels module
* `send_mail` function in `caosadvancedtools.serverside.helper` module
- New class to collect possible problems with the data model
- New class for checking and importing tables
- Function to get a file path to a shared resource directory
- Function to setup logging appropriate for server side scripts with webui 
  output
- New class for collecting information for exporting tables, e.g., to
  metadata repositories
- new name parsing
- new test for software folder structure
- new assure_name_is function
- two utility functions when working with files: NameCollector and 
  get_file_via_download
- Automated documentation builds: `make doc`
- Crawler documentation
- Proof-of-concept integration with Bloxberg.
- Introduce a cfood that can create a Record structure based on the contents of a hdf5 file
  h5py is now an optional dependency

### Changed ###

- identifiables of single CFoods are now treated one after the other. This 
  allows them to have dependencies among each other if they are ordered 
  correctly
- identifiables must have at least one property or a name
* `caosadvancedtools.serverside.helper.init_data_model` also checks the role
  and data type of entities.
* The `caosadvancedtools.table_importer.date_converter` now actually returns
  `datetime.date` instance. A new
  `caosadvancedtools.table_importer.datetime_converter` replaces the old
  `date_converter` and returns a `datetime.datetime` instance.
- The suppression module is now a logging filter.
- The WebUIHandler is now a python logging formatter.
- instead of `get_entity`, type-specific functions are used in
  `cfood.py` when the type of the entity in question is known.
- Logger is used instead of `print` for errors in `crawler.py`.
- complies with new exception handling, i.e., TransactionErros with
  children being raised in all cases of EntityErrors (see
  [#32](https://gitlab.com/caosdb/caosdb-pylib/-/issues/32) in
  caosdb-pylib)
- `caosadvancedtools.cfood.assure_object_is_in_list` conducts in-place
  updates if no `to_be_updated` object is supplied.

### Deprecated ###

### Removed ###

### Fixed ###
- An exception in collect_information does no longer lead to a break down.
- Removed dependency on discontiued xlrd version
* Fixed an issue where `caosadvancedtools.cache.UpdateCache` would
  cause an `sqlite3.IntegrityError` if more than one change was cached
  for the same entity.
* #40 Insertion of identifiables with missing obligatory properties

### Security ###

## [0.2.0] - 2020-05-28 ##

### Added
- everything

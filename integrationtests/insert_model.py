#!/usr/bin/env python3
import caosdb as db
import h5py
from caosadvancedtools.cfoods.h5 import H5CFood
from caosadvancedtools.models.data_model import DataModel
from caosadvancedtools.models.parser import parse_model_from_yaml

model = parse_model_from_yaml("model.yml")
model.sync_data_model(noquestion=True)

if len(db.execute_query("FIND Property alias")) == 0:
    al = db.Property(name="alias")
    al.add_parent(name="name")
    al.insert()

h5model = db.Container()
h5file = h5py.File('extroot/ExperimentalData/2010_TestProject/2019-02-03/hdf5_dummy_file.hdf5', 'r')
H5CFood.create_structure(h5file, create_recordTypes=True, collection=h5model,
                         root_name="ExampleH5")
print(h5model)
h5model = DataModel(h5model)
h5model.sync_data_model(noquestion=True)

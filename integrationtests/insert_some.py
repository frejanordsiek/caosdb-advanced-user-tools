#!/usr/bin/env python3
import caosdb as db
from caosadvancedtools.scifolder.experiment_cfood import dm

# This inserts two identifiables. When no dependencies are possible among
# identifiables, it should not be possible to find both: the experiment
# identifiable would for example not reference the correct project Record
project = db.Record(name='2010_TestProject')
project.add_parent(name=dm.Project)
project.insert()

pers = db.Record()
pers.add_parent("Person")
pers.add_property("lastname", "Wood")
pers.add_property("firstname", "Tom")
pers.insert()

experiment = db.Record()
experiment.add_parent(name=dm.Experiment)
experiment.description = "Something."
experiment.add_property(
    name=dm.date, value='2019-02-04')
experiment.add_property(name=dm.Project, value=project)
experiment.add_property(
        name="identifier", value="empty_identifier")
experiment.add_property(
        name="responsible", value=pers)
experiment.insert(flags={"force-missing-obligatory": "ignore"})

#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2021 IndiScale GmbH <www.indiscale.com>
# Copyright (C) 2021 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

"""
An exemplary definition of a HDF5 CFood for integration testing
"""

import caosdb as db
from caosadvancedtools.cfoods.h5 import H5CFood
from caosadvancedtools.scifolder import ExperimentCFood
from caosadvancedtools.scifolder.generic_pattern import readme_pattern


class ExampleH5CFood(H5CFood):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.root_name = "ExampleH5"

    @staticmethod
    def get_re():
        return ExperimentCFood.get_re()[:-len(readme_pattern)] + r".*\.hdf5"

    def create_identifiables(self):
        super().create_identifiables()
        self.identifiable_root = db.Record()
        self.identifiable_root.add_property("hdf5File", self.crawled_file)
        self.identifiable_root.add_parent("ExampleH5")
        self.identifiables.append(self.identifiable_root)

    def special_treatment(self, key, value, dtype):
        if key == "attr_data_root":
            return "single_attribute", value, dtype

        return key, value, dtype

#!/usr/bin/env python3
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2018 Research Group Biomedical Physics,
# Max-Planck-Institute for Dynamics and Self-Organization Göttingen
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
#

import argparse
import logging
import sys
from argparse import RawTextHelpFormatter

import caosdb as db
from caosadvancedtools.cfood import fileguide
from caosadvancedtools.crawler import FileCrawler
from caosadvancedtools.guard import INSERT, UPDATE
from caosadvancedtools.scifolder import (AnalysisCFood, ExperimentCFood,
                                         PublicationCFood, SimulationCFood,
                                         SoftwareCFood)

from example_hdf5cfood import ExampleH5CFood

try:
    from sss_helper import get_argument_parser, print_success
except ModuleNotFoundError:
    def get_argument_parser():
        return argparse.ArgumentParser()

    def print_success(text):
        print("Success: " + text)


def get_parser():
    # TODO allow to pass something here?
    # description=__doc__, formatter_class=RawTextHelpFormatter
    # with SSS this default parser will have SSS apropriate arguments.
    parser = get_argument_parser()
    parser.add_argument("path",
                        help="the subtree of files below the given path will "
                        "be considered. Use '/' for everything.")
    parser.add_argument("-a", "--authorize-run", action='append',
                        help="supply the id of the run that you want to"
                        " authorize")

    return parser


def access(path):
    return "extroot" + path


if __name__ == "__main__":
    logger = logging.getLogger("caosadvancedtools")
    logger.addHandler(logging.StreamHandler(sys.stdout))
    conlogger = logging.getLogger("connection")
    conlogger.setLevel(level=logging.ERROR)
    logger.setLevel(level=logging.DEBUG)
    fileguide.access = access
    parser = get_parser()
    args = parser.parse_args()

    # assuming SSS

    if hasattr(args, "auth_token") and args.auth_token:
        db.configure_connection(password_method="auth_token",
                                auth_token=args.auth_token)

    logger.info("Starting query...")
    files = FileCrawler.query_files(args.path)
    logger.info("Query done...")
    config = db.configuration.get_config()
    c = FileCrawler(files=files, use_cache=True,
                    interactive=False, hideKnown=False,
                    cfood_types=[ExperimentCFood, AnalysisCFood, SoftwareCFood,
                                 PublicationCFood, SimulationCFood,
                                 ExampleH5CFood
                                 ])

    if args.authorize_run:
        for run_id in args.authorize_run:
            c.update_authorized_changes(run_id)

    c.crawl(security_level=INSERT, path=args.path)
    print_success('Crawler finished')

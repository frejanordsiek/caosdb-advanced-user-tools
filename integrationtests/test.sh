#!/bin/bash
if [ "$1" != "--force" ]
then
    echo "Warning: For these tests, the whole database will be deleted. Do you want to proceed? (yes/Exit)"
    read safety
    if [ -z $safety ]
    then
        echo "Exiting..."
        exit 0
    elif [ $safety != "yes" ]
    then
        echo "Exiting..."
        exit 0
    fi
fi
OUT=/tmp/crawler.output
ls 
cat pycaosdb.ini
rm -rf cache.db
set -e
echo "Clearing database"
python3 clear_database.py
echo "Testing crawler without cfoods"
python3 -m pytest test_crawler_basics.py
echo "Testing caching"
python3 -m pytest test_cache.py
echo "Testing models"
python3 -m pytest test_data_model.py
echo "Testing cfood functionality"
python3 -m pytest test_assure_functions.py

echo "Filling the database"
./filldb.sh
echo "Testing the crawler database"
python3 -m pytest test_crawler_with_cfoods.py
echo "make a change"
cd extroot
egrep -liRZ 'A description of another example' . | xargs -0 -l sed -i -e 's/A description of another example/A description of this example/g'
# remove a file to check that this does not lead to a crawler crash
mv DataAnalysis/2010_TestProject/2019-02-03_something/README.xlsx DataAnalysis/2010_TestProject/2019-02-03_something/README.xlsx_back
cd ..
echo "run crawler"
./crawl.py  / | tee $OUT
# rename the moved file
mv extroot/DataAnalysis/2010_TestProject/2019-02-03_something/README.xlsx_back extroot/DataAnalysis/2010_TestProject/2019-02-03_something/README.xlsx
# check whether there was something UNAUTHORIZED
grep "There where unauthorized changes" $OUT
# get the id of the run which is the last field of the output string
RUN_ID=$(grep "run id:" $OUT | awk '{ print $NF }')
echo $RUN_ID
echo "run crawler again"
echo "./crawl.py -a $RUN_ID /"
./crawl.py -a $RUN_ID / | tee  $OUT
set +e
if grep "There where unauthorized changes" $OUT
then 
    exit 1
fi
set -e
echo "undo changes"
cd extroot
egrep -liRZ 'A description of this example' . | xargs -0 -l sed -i -e 's/A description of this example/A description of another example/g'
cd ..
python3 test_table.py
# TODO the following test deletes lots of the data inserted by the crawler
echo "Testing im and export"
python3 test_im_und_export.py
# Better safe than sorry:
python3 clear_database.py

# Test correct display of data model errors:
echo "Testing recognition of data model problems ... "
python3 -m pytest test_datamodel_problems.py
python3 -m pytest test_crawl_with_datamodel_problems.py

echo "Testing table export"
python3 -m pytest test_base_table_exporter_integration.py

# Obsolete due to teardown in the above test.
# echo "/n/n/n YOU NEED TO RESTART THE SERVER TO REDO TESTS!!!"

#!/usr/bin/env python3
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

import logging

import caosdb as db
import pandas as pd

from caosadvancedtools.crawler import TableCrawler
from caosadvancedtools.guard import UPDATE

if __name__ == "__main__":
    logger = logging.getLogger("caosadvancedtools")
    conlogger = logging.getLogger("connection")
    conlogger.setLevel(level=logging.ERROR)
    logger.setLevel(level=logging.DEBUG)

    table = pd.read_csv("example_table.csv")

    assert 0 == len(db.execute_query("FIND Person with firstname=Henrik"))
    first = table.loc[table.firstName == "Henrik"]
    tcr = TableCrawler(table=first, unique_cols=["firstName", "lastName"],
                       recordtype="Person", interactive=False)
    tcr.crawl(security_level=UPDATE)
    assert 1 == len(db.execute_query("FIND Person with firstname=Henrik"))
    tcr = TableCrawler(table=table, unique_cols=["firstName", "lastName"],
                       recordtype="Person", interactive=False)
    tcr.crawl(security_level=UPDATE)
    assert 1 == len(db.execute_query("FIND Person with firstname=Henrik"))
    assert 1 == len(db.execute_query("FIND Person with firstname=Max"))

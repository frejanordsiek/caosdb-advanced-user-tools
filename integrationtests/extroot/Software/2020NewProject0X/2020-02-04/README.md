---
responsible:	
- Some Responsible
- Responsible, No, MPI DS
description: 	A description of this example analysis.

sources:
- file:	"/ExperimentalData/2010_TestProject/2019-02-03/*.dat"
  description:  an example reference to a results file

sourceCode: plot.py
binaries:
- file: example.deb
  description: the binary file
...

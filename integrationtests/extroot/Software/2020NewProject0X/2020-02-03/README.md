---
responsible:	
- Only Responsible MPI DS
description: 	A description of this example analysis.

sources:
- file:	"/ExperimentalData/2010_TestProject/2019-02-03/*.dat"
  description:  an example reference to a results file

sourceCode:
- file: plot.py
  description: a plotting script
- file: calc.py
  description: a calc script
...

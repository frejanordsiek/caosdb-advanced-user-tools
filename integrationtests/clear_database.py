# -*- coding: utf-8 -*-
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2020 Florian Spreckelsen <f.spreckelsen@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public
# License along with this program. If not, see
# <https://www.gnu.org/licenses/>.
#
# ** end header
#
"""Clear the database before and after the integration tests."""
import caosdb as db


def clear_all():
    """First remove Records, then RecordTypes, then Properties, finally
    files. Since there may be no entities, execute all deletions
    without raising errors.

    """
    db.execute_query("FIND Record").delete(
        raise_exception_on_error=False)
    db.execute_query("FIND RecordType").delete(
        raise_exception_on_error=False)
    db.execute_query("FIND Property").delete(
        raise_exception_on_error=False)
    db.execute_query("FIND File").delete(
        raise_exception_on_error=False)


if __name__ == "__main__":
    clear_all()

$(document).ready(function(){	
	Global.init();
});

var Global = {

	init: function(){
		var globalData 	= $("#global_data");

		// firstLogin
		this.firstLogin							= (globalData.attr("data-firstlogin") == "true") ? true : false;
		
		// currentUser
		this.userShowHiddenItems				= (globalData.attr("data-user-show-hidden-items") == "true") ? true : false;
		this.userId								= globalData.attr("data-user-id");
		this.userProfilePictureHash				= globalData.attr("data-user-profilePictureHash");
		this.userFirstName						= globalData.attr("data-user-firstName");
		this.userLastName						= globalData.attr("data-user-lastName");
		this.userProfileComplete				= (globalData.attr("data-user-profileComplete") == "true") ? true : false;
		this.orderASC							= (globalData.attr("data-user-orderASC") == "true" ) ? true : false;

		// currentProject
		this.projectId 							= globalData.attr("data-project-id");
		this.projectIsTemplate 					= (globalData.attr("data-project-is-template") == "true") ? true : false;
		this.projectIsHidden 					= (globalData.attr("data-project-is-hidden") == "true") ? true : false;

		// currentGroup
		this.groupId 							= globalData.attr("data-group-id");
		this.groupType 							= globalData.attr("data-group-type");
		this.groupSettingsPreventDeleteProject 	= (globalData.attr("data-group-settings-prevent-delete-project") == "true") ? true : false;

		// currentGroupMemberAdminLevels
		this.groupMemberAdminLevels 			= [];
		var groupMemberAdminLevelsStr = globalData.attr("data-group-member-admin-levels");
		if(groupMemberAdminLevelsStr) {
			var arr = groupMemberAdminLevelsStr.split(",");
			for(var i=0; i<arr.length; i++) {
				var trimmed = $.trim(arr[i]);
				if(trimmed != "") {
					this.groupMemberAdminLevels.push(trimmed);
				}
			}
		}
		
		this.installedApps 			= [];
		var installedAppsStr = globalData.attr("data-installed-apps");
		if(installedAppsStr) {
			var arr = installedAppsStr.split(",");
			for(var i=0; i<arr.length; i++) {
				var trimmed = $.trim(arr[i]);
				if(trimmed != "") {
					this.installedApps.push(trimmed);
				}
			}
		}
		
		this.buildNumber = globalData.attr("data-build-number");

		this.viewname = $("#data_element").attr("data-viewname");

	},
	
	isGroupAdmin: function(subgroupId) {
		for (var i = 0; i < this.groupMemberAdminLevels.length; i++) {
			if(this.groupMemberAdminLevels[i] == subgroupId) {
				return true;
			}
		}
		return false;
	},
	
	isAppInstalled: function(appKey) {
		for (var i = 0; i < this.installedApps.length; i++) {
			if(this.installedApps[i] == appKey) {
				return true;
			}
		}
		return false;
	}
	
}

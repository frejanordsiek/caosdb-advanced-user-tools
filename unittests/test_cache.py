#!/usr/bin/env python
# encoding: utf-8
#
# ** header v3.0
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2019 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#
# ** end header
import os
import unittest
from copy import deepcopy
from tempfile import NamedTemporaryFile

import caosdb as db
from caosadvancedtools.cache import Cache, cleanXML
from lxml import etree


class CacheTest(unittest.TestCase):
    def setUp(self):
        self.cache = Cache(db_file=NamedTemporaryFile(delete=False).name)
        self.cache.create_cache()

    def test_hash(self):
        ent = db.Record()
        assert isinstance(Cache.hash_entity(ent), str)
        assert (Cache.hash_entity(ent) !=
                Cache.hash_entity(db.Record().add_parent("lol")))

    def test_insert(self):
        ent = db.Record()
        ent2 = db.Record()
        ent2.add_parent(name="Experiment")
        ent_hash = Cache.hash_entity(ent)
        ent2_hash = Cache.hash_entity(ent2)
        self.cache.insert(ent2_hash, 1235)
        assert isinstance(self.cache.check_existing(ent2_hash), int)
        assert self.cache.check_existing(ent_hash) is None

    def test_hirarchy(self):
        assert isinstance(db.Record(), db.Entity)

    def tearDown(self):
        os.remove(self.cache.db_file)

    def test_update_ids_from_cache(self):
        ent = db.Record()
        ent2 = db.Record()
        ent2.add_parent(name="Experiment")
        ent3 = db.Record()
        ent3.add_parent(name="Analysis")
        test_id = 2353243
        self.cache.insert(Cache.hash_entity(ent2), test_id)
        entities = [ent, ent2, ent3]
        hashes = self.cache.update_ids_from_cache(entities)
        self.assertEqual(ent2.id, test_id)

        # test
        ent.id = 1001
        ent3.id = 1003
        self.cache.insert_list(hashes, entities)
        self.assertEqual(self.cache.check_existing(hashes[0]), 1001)
        self.assertEqual(self.cache.check_existing(hashes[2]), 1003)

    def test_clean(self):
        xml = etree.XML(
            """\
            <Entities>
  <TransactionBenchmark>
    </TransactionBenchmark>
  <RecordType id="110" name="Guitar">
    <Version id="eb8c7527980e598b887e84d055db18cfc3806ce6" head="true"/>
    <Parent id="108" name="MusicalInstrument" flag="inheritance:OBLIGATORY,"/>
    <Property id="106" name="electric" datatype="BOOLEAN" importance="RECOMMENDED" flag="inheritance:FIX"/>
  </RecordType>
    </Entities>
""")
        cleanXML(xml)
        assert len(xml.findall('TransactionBenchmark')) == 0

import unittest

import caosdb as db
import pytest
from caosadvancedtools.models.data_model import DataModel


class DataModelTest(unittest.TestCase):

    def tearDown(self):
        try:
            tests = db.execute_query("FIND test*")
            tests.delete()
        except Exception:
            pass

    def test_collecting(self):
        maintained = {"one": db.RecordType(name="TestRecord").add_property(
            name="testproperty"),
                      "two": db.Property(name="testproperty", datatype=db.INTEGER)}
        dm = DataModel(maintained.values())
        col = dm.collect_entities()
        names = [e.name for e in col]
        assert "TestRecord" in names
        assert "testproperty" in names

    # TODO this seems to require integration test
    @pytest.mark.xfail
    def test_get_existing_entities(self):
        db.RecordType(name="TestRecord").insert()
        c = db.Container().extend([
            db.Property(name="testproperty"),
            db.RecordType(name="TestRecord")])
        exist = DataModel.get_existing_entities(c)
        assert len(exist) == 1
        assert exist[0].name == "TestRecord"

    def test_sync_ids_by_name(self):
        container = db.Container().extend([db.RecordType(name="TestRecord"),
                                           db.RecordType(name="TestRecord2"),
                                           ])

        # assign negative ids
        container.to_xml()
        l1 = DataModel(container)

        rt = db.RecordType(name="TestRecord")
        rt.id = 1002
        rt2 = db.RecordType(name="TestRecordNonono")
        rt2.id = 1000
        l2 = [rt2, rt]
        DataModel.sync_ids_by_name(l1, l2)
        assert l1["TestRecord"].id == rt.id
        assert l1["TestRecord2"].id < 0

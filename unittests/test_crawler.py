#!/usr/bin/env python
# encoding: utf-8
#
# This file is a part of the CaosDB Project.
#
# Copyright (C) 2020 Indiscale GmbH <info@indiscale.com>
# Copyright (C) 2020 Henrik tom Wörden <h.tomwoerden@indiscale.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
#

import re
import unittest

import caosdb as db
from caosadvancedtools.crawler import Crawler


class CrawlerTest(unittest.TestCase):
    def test_ident_query(self):
        ident = db.Record(name="coolrec")
        self.assertRaises(ValueError, Crawler.create_query_for_identifiable,
                          ident)
        ident.add_parent(name="RT")
        qs = Crawler.create_query_for_identifiable(ident)
        assert qs == "FIND Record RT WITH name='coolrec'"
        ident.add_property(name="p", value=5)
        qs = Crawler.create_query_for_identifiable(ident)
        assert qs == "FIND Record RT WITH name='coolrec' AND'p'='5' "

        ident = db.Record()
        ident.add_parent(name="RT")
        ident.add_property(name="p", value=[2345, db.Record(id=234567)],
                           datatype=db.LIST("RT2"))
        qs = Crawler.create_query_for_identifiable(ident)
        assert qs == "FIND Record RT WITH references 2345 AND references 234567 "
        ident = db.Record()
        ident.add_parent(name="RT")
        self.assertRaises(ValueError, Crawler.create_query_for_identifiable,
                          ident)

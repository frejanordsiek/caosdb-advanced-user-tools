# Getting started

## Download
The recommended way is:
```
# Clone the repository:
git clone 'https://gitlab.com/caosdb/caosdb-advanced-user-tools'
```

## Dependencies
Dependencies will be installed automatically if you use the below described procedure.
- `caosdb>=0.4.0`                                      
- `openpyxl>=3.0.0`
- `xlrd>=1.2.0`
-  `pandas>=1.2.0`
- `numpy>=1.17.3`

If you want to use the optional h5-crawler the following dependencies will be installed additionally:
- `h5py>=3.3.0`

For testing:
- `tox`


## Installation
- `pip install . --user`
- `pip install tox --user`

Optional h5-crawler:
- `pip install .[h5-crawler] --user`

## Run Unit Tests
`tox`

## Run Integration Tests Locally

1. Change directory to `integrationtests/`.
2. Mount `extroot` to the folder that will be used as
   extroot. E.g. `sudo mount -o bind extroot
   ../../caosdb-deploy/profiles/empty/paths/extroot` (or whatever path
   the extroot of the empty profile to be used is located at).
3. Start (or restart) an empty (!) CaosDB instance (with the mounted extroot).
   The database will be cleared during testing, so it's important to use
   an empty instance.
   Make sure your configuration for the python caosdb module is correct and
   allows to connect to the server.
4. Run `test.sh`.  Note that this may modify content of the `integrationtest/extroot/` directory.

## Code Formatting
`autopep8 -i -r ./`

## Documentation #

Build documentation in `build/` with `make doc`.

### Requirements ##

- `sphinx`
- `sphinx-autoapi`
- `recommonmark >= 0.6.0`

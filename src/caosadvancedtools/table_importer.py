#!/usr/bin/env python
# encoding: utf-8
#
# Copyright (C) 2020 Henrik tom Wörden
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""
This module allows to read table files like tsv and xls. They are converted to
a Pandas DataFrame and checked whether they comply with the rules provided.
For example, a list of column names that have to exist can be provided.

This module also implements some converters that can be applied to cell
entries.

Those converters can also be used to apply checks on the entries.
"""

import logging
import pathlib
from datetime import datetime

import numpy as np
import pandas as pd
from xlrd import XLRDError

from caosadvancedtools.utils import check_win_path

from .datainconsistency import DataInconsistencyError
from .suppressKnown import SuppressKnown

logger = logging.getLogger(__name__)


def assure_name_format(name):
    """
    checks whether a string can be interpreted as 'LastName, FirstName'
    """
    name = str(name)

    if len(name.split(",")) != 2:
        raise ValueError("Name field should be 'LastName, FirstName'."
                         "The supplied value was '{}'.".format(name))

    return name


def yes_no_converter(val):
    """
    converts a string to True or False if possible.

    Allowed filed values are yes and no.
    """

    if str(val).lower() == "yes":
        return True
    elif str(val).lower() == "no":
        return False
    else:
        raise ValueError(
            "Field should be 'Yes' or 'No', but is '{}'.".format(val))


def datetime_converter(val, fmt="%Y-%m-%d %H:%M:%S"):
    """ if the value is already a datetime, it is returned otherwise it
    converts it using format string
    """

    if isinstance(val, datetime):
        return val
    else:
        return datetime.strptime(val, fmt)


def date_converter(val, fmt="%Y-%m-%d"):
    """ if the value is already a datetime, it is returned otherwise it
    converts it using format string
    """

    return datetime_converter(val, fmt=fmt).date()


def incomplete_date_converter(val, fmts={"%Y-%m-%d": "%Y-%m-%d",
                                         "%Y-%m": "%Y-%m", "%Y": "%Y"}):
    """ if the value is already a datetime, it is returned otherwise it
    converts it using format string

    Parameters
    ----------
    val : str
        Candidate value for one of the possible date formats.
    fmts : dict, optional
        Dictionary containing the possible (incomplete) date formats:
        keys are the formats into which the input value is tried to be
        converted, values are the possible input formats.
    """

    for to, fro in fmts.items():
        try:
            date = datetime.strptime(val, fro)

            return date.strftime(to)

        except ValueError:
            pass
    raise RuntimeError(
        "Value {} could not be converted with any format string".format(
            val))


def win_path_list_converter(val):
    """
    checks whether the value looks like a list of windows paths and converts
    it to posix paths
    """

    if pd.isnull(val):
        return []
    paths = val.split(",")

    return [win_path_converter(p) for p in paths]


def win_path_converter(val):
    """
    checks whether the value looks like a windows path and converts it to posix
    """

    if not check_win_path(val):
        raise ValueError(
            "Field should be a Windows path, but is\n'{}'.".format(val))
    path = pathlib.PureWindowsPath(val)

    return path.as_posix()


class TSVImporter(object):
    def __init__(self, converters, obligatory_columns=[], unique_columns=[]):
        raise NotImplementedError()


class XLSImporter(object):
    def __init__(self, converters, obligatory_columns=None, unique_keys=None):
        """
        converters: dict with column names as keys and converter functions as
                    values
                    This dict also defines what columns are required to exist
                    throught the existing keys. The converter functions are
                    applied to the cell values. They should also check for
                    ValueErrors, such that a separate value check is not
                    necessary.
        obligatory_columns: list of column names, optional
                            each listed column must not have missing values
        unique_columns : list of column names that in
                            combination must be unique; i.e. each row has a
                            unique combination of values in those columns.
        """
        self.sup = SuppressKnown()
        self.required_columns = list(converters.keys())
        self.obligatory_columns = [] if obligatory_columns is None else obligatory_columns
        self.unique_keys = [] if unique_keys is None else unique_keys
        self.converters = converters

    def read_xls(self, filename, **kwargs):
        """
        converts an xls file into a Pandas DataFrame.

        The converters of the XLSImporter object are used.

        Raises: DataInconsistencyError
        """
        try:
            xls_file = pd.io.excel.ExcelFile(filename)
        except XLRDError as e:
            logger.warning(
                "Cannot read \n{}.\nError:{}".format(filename,
                                                     str(e)),
                extra={'identifier': str(filename),
                       'category': "inconsistency"})
            raise DataInconsistencyError(*e.args)

        if len(xls_file.sheet_names) > 1:
            # Multiple sheets is the default now. Only show in debug
            logger.debug(
                "Excel file {} contains multiple sheets. "
                "All but the first are being ignored.".format(filename))

        try:
            df = xls_file.parse(converters=self.converters, **kwargs)
        except Exception as e:
            logger.warning(
                "Cannot parse {}.".format(filename),
                extra={'identifier': str(filename),
                       'category': "inconsistency"})
            raise DataInconsistencyError(*e.args)

        self.check_columns(df, filename=filename)
        df = self.check_missing(df, filename=filename)

        if len(self.unique_keys) > 0:
            df = self.check_unique(df, filename=filename)

        return df

    def check_columns(self, df, filename=None):
        """
        checks whether all required columns, i.e. columns for which converters
        were defined exist.

        Raises: DataInconsistencyError
        """

        for col in self.required_columns:
            if col not in df.columns:
                errmsg = "Column '{}' missing in ".format(col)
                errmsg += ("\n{}.\n".format(filename) if filename
                           else "the file.")
                errmsg += "Stopping to treat this file..."
                logger.warning(
                    errmsg,
                    extra={'identifier': str(filename),
                           'category': "inconsistency"})
                raise DataInconsistencyError(errmsg)

    def check_unique(self, df, filename=None):
        """
        Check whether value combinations that shall be unique for each row are
        unique.

        If a second row is found, that uses the same combination of values as a
        previous one, the second one is removed.
        """
        df = df.copy()
        uniques = []

        for unique_columns in self.unique_keys:
            subtable = df[list(unique_columns)]

            for index, row in subtable.iterrows():
                element = tuple(row)

                if element in uniques:
                    errmsg = (
                        "The {}. row contains the values '{}'.\nThis value "
                        "combination should be unique, but was used in a previous "
                        "row in\n").format(index+1, element)
                    errmsg += "{}.".format(filename) if filename else "the file."
                    errmsg += "\nThis row will be ignored!"

                    logger.warning(errmsg, extra={'identifier': filename,
                                                  'category': "inconsistency"})
                    df = df.drop(index)
                else:
                    uniques.append(element)

        return df

    def check_missing(self, df, filename=None):
        """
        Check in each row whether obligatory fields are empty or null.

        Rows that have missing values are removed.
        """
        df = df.copy()

        for index, row in df.iterrows():
            # if none of the relevant information is given, skip

            if np.array([pd.isnull(row.loc[key]) for key in
                         self.obligatory_columns]).all():

                df = df.drop(index)

                continue

            # if any of the relevant information is missing, report it

            i = 0
            okay = True

            while okay and i < len(self.obligatory_columns):
                key = self.obligatory_columns[i]
                i += 1

                if pd.isnull(row.loc[key]):
                    errmsg = (
                        "Required information is missing ({}) in {}. row"
                        " (without header) of "
                        "file:\n{}".format(key, index+1, filename))

                    logger.warning(errmsg, extra={'identifier': filename,
                                                  'category': "inconsistency"})
                    df = df.drop(index)

                    okay = False

        return df

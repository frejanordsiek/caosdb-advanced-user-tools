# Use docker as parent image
FROM docker:19.03.0

# http://bugs.python.org/issue19846
ENV LANG C.UTF-8

# install dependencies
RUN apk add --no-cache py3-pip python3 python3-dev gcc make \
    git bash curl gettext  py3-requests 
RUN apk add --no-cache libffi-dev openssl-dev libc-dev libxslt libxslt-dev \
    libxml2 libxml2-dev

# install rust (needed for compiling a docker-compose dependency)
# This is necessary until alpine comes with an up to date RUST
# copied from https://github.com/rust-lang/docker-rust/blob/bbc7feb12033da3909dced4e88ddbb6964fbc328/1.50.0/alpine3.13/Dockerfile

ENV RUSTUP_HOME=/usr/local/rustup \
    CARGO_HOME=/usr/local/cargo \
    PATH=/usr/local/cargo/bin:$PATH \
    RUST_VERSION=1.50.0

RUN set -eux; \
    apkArch="$(apk --print-arch)"; \
    case "$apkArch" in \
        x86_64) rustArch='x86_64-unknown-linux-musl'; rustupSha256='05c5c05ec76671d73645aac3afbccf2187352fce7e46fc85be859f52a42797f6' ;; \
        aarch64) rustArch='aarch64-unknown-linux-musl'; rustupSha256='6a8a480d8d9e7f8c6979d7f8b12bc59da13db67970f7b13161ff409f0a771213' ;; \
        *) echo >&2 "unsupported architecture: $apkArch"; exit 1 ;; \
    esac; \
    url="https://static.rust-lang.org/rustup/archive/1.23.1/${rustArch}/rustup-init"; \
    wget "$url"; \
    echo "${rustupSha256} *rustup-init" | sha256sum -c -; \
    chmod +x rustup-init; \
    ./rustup-init -y --no-modify-path --profile minimal --default-toolchain $RUST_VERSION --default-host ${rustArch}; \
    rm rustup-init; \
    chmod -R a+w $RUSTUP_HOME $CARGO_HOME; \
    rustup --version; \
    cargo --version; \
    rustc --version;

RUN pip3 install docker-compose==1.25

# Script for waiting on LA server
COPY wait-for-it.sh /opt/caosdb/wait-for-it.sh

# Runtime settings
WORKDIR /opt/caosdb
RUN mkdir -p /opt/caosdb/build_docker/
CMD /bin/bash

# python client
ADD https://gitlab.com/api/v4/projects/13656973/repository/branches/dev \
	  pylib_version.json
RUN pip3 install git+https://gitlab.com/caosdb/caosdb-pylib 
